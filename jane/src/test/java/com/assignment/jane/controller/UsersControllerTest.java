package com.assignment.jane.controller;

import com.assignment.jane.model.UsersModel;
import com.assignment.jane.model.UsersRequest;
import com.assignment.jane.repository.UsersReposiotory;
import com.assignment.jane.service.UsersService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;


//@Slf4j
//@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = UsersController.class)
@ActiveProfiles("test")
public class UsersControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UsersService usersService;
    @MockBean
    private UsersReposiotory usersReposiotory;



    @BeforeEach
    void setUp() {
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();
        UsersRequest request = UsersRequest.builder()
                .name("jann")
                .build();

    }

    @Test
    public void createData() throws Exception{

        when(usersService.createData(any(UsersModel.class))).thenAnswer((invocation) -> invocation.getArgument(0));

        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();
//        when(usersService.createData(expectedUser)).thenReturn();
        this.mockMvc.perform(post("/api/jane/createData")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(expectedUser)))
                .andExpect(status().isOk());

    }

    @Test
    void findById() throws Exception{
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();
        when(usersService.findById(3)).thenReturn(Optional.of(expectedUser));
//        String[] id = new String[1];
        this.mockMvc.perform(get("/api/jane/id")
                        .param("id", "3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(expectedUser.getName())))
                .andExpect(jsonPath("$.salary", is(expectedUser.getSalary())));

    }

    @Test
    void getAllData() throws Exception{
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();


        when(usersService.getAllData()).thenReturn(List.of(expectedUser));

        this.mockMvc.perform(get("/api/jane/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()",is(List.of(expectedUser).size())));
    }

    @Test
    void updateAllById() throws Exception{
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();

        given(usersService.findById(3)).willReturn(Optional.of(expectedUser));
        given(usersService.updateAllById(any(UsersModel.class))).willAnswer((invocation) -> invocation.getArgument(0));

        this.mockMvc.perform(put("/api/jane/id/updateall")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(expectedUser)))
                .andExpect(status().isOk());



    }

    @Test
    void updateById() throws Exception{
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();
        UsersRequest request = UsersRequest.builder()
                .name("janmm")
                .build();

        doReturn(Optional.of(expectedUser))
                .when(usersService)
                .findById(any());

        this.mockMvc.perform(patch("/api/jane/id/update")
                        .param("id", String.valueOf(3))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk());

    }

    @Test
    void deleteById() throws Exception{
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();
        when(usersService.deleteById(3)).thenReturn(Optional.of(expectedUser));
        this.mockMvc.perform(patch("/api/jane/id/delete")
                        .param("id", "3"))
                .andExpect(status().isOk());

    }
}