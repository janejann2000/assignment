CREATE TABLE users(
    id SERIAL PRIMARY KEY ,
    name varchar(255),
    salary double precision
);