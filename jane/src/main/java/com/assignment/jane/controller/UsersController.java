package com.assignment.jane.controller;


import com.assignment.jane.model.UsersModel;
import com.assignment.jane.model.UsersRequest;
import com.assignment.jane.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/jane")
public class UsersController {
    @Autowired
    public UsersService usersService;
    public UsersModel usersModel;
    /*@GetMapping(value ={"","/assignment1"})
    public  String Controller(){
        return "Hello Backend Team";
    }*/

    @PostMapping(value="/createData")
    public void createData(@RequestBody UsersModel usersModel){
        usersService.createData(usersModel);
    }

    @GetMapping(value = "/id")
    public Optional<UsersModel> findById(@RequestParam Integer id){
        Optional<UsersModel> data = usersService.findById(id);
        return  data;
    }

    @GetMapping(value = "/all")
    public List<UsersModel> getAllData(){
        return usersService.getAllData();
    }

    @PutMapping(value = "/id/updateall")
    public String updateAllById(@RequestBody UsersModel usersModel) throws ValidationException {
        try{
            if(usersModel.getName().isEmpty() || usersModel.getName() == " " || usersModel.getSalary() == null){
                throw new ValidationException("Error 422");
            }
            if(usersModel.getName().length() > 255){
                throw new SQLException("Name too long");
            }
            usersService.updateAllById(usersModel);
        }catch (SQLException e){
            return (e.getMessage());
        }catch (ValidationException e){
            return (e.getMessage() );
        }
        return "Update Success";
    }

    @PatchMapping(value = "/id/update")
    public String updateById(@RequestParam(value = "id", required = true) Integer id, @RequestBody UsersRequest usersRequest){
        try{
            if (!usersRequest.getName().isEmpty() && usersRequest.getName().length() > 255) {
                throw new SQLException("Name too long");
            }
            usersService.updateById(id, usersRequest);
        }catch (SQLException e){
            return (e.getMessage());
        }
        return "Update Success";
    }

    @PatchMapping(value = "/id/delete")
    public void deleteById(@RequestParam(value = "id", required = true) Integer id){
        usersService.deleteById(id);
    }

}
