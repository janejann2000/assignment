CREATE TABLE Transaction(
    userName varchar(255),
    userId int,
    transactionDate date,
    transactionAmount double precision
);