package com.assignment.jane.batch;

import com.assignment.jane.model.UsersModel;
import org.springframework.stereotype.Component;
import org.springframework.batch.item.ItemProcessor;

import java.util.HashMap;
import java.util.Map;

@Component
public class Processor implements ItemProcessor<UsersModel, UsersModel> {

    private static final Map<String, String> DEPT_NAMES =
            new HashMap<>();

    public Processor() {
    }

    @Override
    public UsersModel process(UsersModel usersModel) throws Exception {
        return usersModel;
    }
}