package com.assignment.jane.repository;

import com.assignment.jane.model.UsersModel;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class UsersRepositoryTest {

    @Mock
    public UsersReposiotory usersReposiotory;

    @Test
    public void findById() {
        UsersModel userModel = UsersModel.builder()
                .id(1)
                .name("pen")
                .salary(5.0)
                .build();
        Optional<UsersModel> users = Optional.of(userModel);
        when(usersReposiotory.findById(any())).thenReturn(users);

        Optional<UsersModel> actualResponse = usersReposiotory.findById(1);

        assertEquals(actualResponse.get().getName(), userModel.getName());
    }

}