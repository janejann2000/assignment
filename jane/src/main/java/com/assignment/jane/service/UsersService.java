package com.assignment.jane.service;

import com.assignment.jane.model.UsersModel;
import com.assignment.jane.model.UsersRequest;
import com.assignment.jane.repository.UsersReposiotory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UsersService {

    @Autowired
    public UsersReposiotory usersReposiotory;

    public UsersModel createData(UsersModel usersModel) {
        return usersReposiotory.save(usersModel);
    }

    public Optional<UsersModel> findById(Integer id) {
        return usersReposiotory.findById(id);
    }

    public List<UsersModel> getAllData() {
        return usersReposiotory.findAll();
    }


    public Optional<UsersModel> deleteById(Integer id) {
        usersReposiotory.deleteById(id);
        return findById(id);
    }

    public UsersModel updateAllById(UsersModel usersModel) {
        return usersReposiotory.save(usersModel);

    }

    public UsersModel updateById(Integer id, UsersRequest usersRequest) {
        Optional<UsersModel> data = usersReposiotory.findById(id);
        if (data.isPresent()) {
            UsersModel usersModel = data.get();
            if (usersRequest.getName() != null && !(usersRequest.getName().isEmpty())) {
                usersModel.setName(usersRequest.getName());
            }
            if (usersRequest.getSalary() != null) {
                usersModel.setSalary(usersRequest.getSalary());
            }
            usersReposiotory.save(usersModel);
        }
        return usersReposiotory.findById(id).get();

    }
}








