package com.assignment.jane.kafka;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
@Slf4j
@Service
public class Consumer {
//    private final Logger logger = LoggerFactory.getLogger(Producer.class);

    @KafkaListener(topics = "users", groupId = "test_group")
    public void consume(String message) throws IOException {
        log.info(String.format("message from kafka -> %s", message));
    }
}
