package com.assignment.jane.repository;

import com.assignment.jane.model.UsersModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersReposiotory extends JpaRepository<UsersModel,Integer> {
    Optional<UsersModel> findById(Integer id);
}

