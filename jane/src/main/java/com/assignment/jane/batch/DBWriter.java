package com.assignment.jane.batch;

import com.assignment.jane.model.UsersModel;
import com.assignment.jane.repository.UsersReposiotory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DBWriter implements ItemWriter<UsersModel> {

    private UsersReposiotory usersRepository;

    @Autowired
    public DBWriter (UsersReposiotory usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void write(List<? extends UsersModel> usersModel) throws Exception{
        System.out.println("Data Saved for Users: " + usersModel);
        usersRepository.saveAll(usersModel);
    }
}