package com.assignment.jane.service;

import com.assignment.jane.model.UsersModel;
import com.assignment.jane.model.UsersRequest;
import com.assignment.jane.repository.UsersReposiotory;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class UsersServiceTest {

    @InjectMocks
    private UsersService usersService;

    @Mock
    private UsersReposiotory usersReposiotory;

    @Test
    public void findById() {
        //given
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();

        doReturn(Optional.of(expectedUser))
                .when(usersReposiotory)//when
                .findById(any());//then

       Optional<UsersModel> actualResponse = usersService.findById(1);
       log.info("{}", actualResponse.get());
       assertTrue(actualResponse.isPresent());
    }

    @Test
    void createData() {
        //given
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();

        doReturn(expectedUser)
                .when(usersReposiotory)//when
                .save(any());//then

        UsersModel actualResponse = usersService.createData(expectedUser);
        log.info("{}", actualResponse);
        assertEquals(actualResponse.getId(),expectedUser.getId());
    }

    @Test
    void getAllData() {
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();

        doReturn(List.of(expectedUser))
                .when(usersReposiotory)//when
                .findAll();//then

        List<UsersModel> actualResponse = usersService.getAllData();
        log.info("{}", actualResponse);
        assertEquals(actualResponse,List.of(expectedUser));
    }

    @Test
    void deleteById() {
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();

        usersService.deleteById(3);
        verify(usersReposiotory,times(1)).deleteById(3);
//        doNothing()
//                .when(usersReposiotory)//when
//                .deleteById(any());//then

//        Optional<UsersModel> actualResponse = usersService.deleteById(3);
//        log.info("{}", usersService.deleteById(3));
//        assertTrue(usersService.deleteById(3).isEmpty());

    }

    @Test
    void updateAllById() {
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();

        doReturn(expectedUser)
                .when(usersReposiotory)//when
                .save(any());//then

        UsersModel actualResponse = usersService.updateAllById(UsersModel
                .builder()
                .id(3)
                .name("jan")
                .salary(Double.valueOf("100"))
                .build());
        log.info("{}", actualResponse);
        assertEquals(actualResponse.getName(),expectedUser.getName());
    }

    @Test
    void updateById() {
        UsersModel expectedUser = UsersModel.builder()
                .id(3)
                .name("jane")
                .salary(Double.valueOf("10000"))
                .build();
        UsersRequest request = UsersRequest.builder()
                .name("jam")
                .build();

        when(usersReposiotory.findById(3))
                .thenReturn(Optional.of(expectedUser));

        usersService.updateById(3,request);
        verify(usersReposiotory,times(1)).save(any());
//        doReturn(expectedUser)
//                .when(usersReposiotory)//when
//                .save(any());//then
//
//        Optional<UsersModel> actualResponse = usersService.updateById(3,request);
//        log.info("{}", actualResponse);
//        assertNotEquals(actualResponse , expectedUser);
    }
}