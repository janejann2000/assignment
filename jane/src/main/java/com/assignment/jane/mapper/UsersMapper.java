package com.assignment.jane.mapper;

import com.assignment.jane.model.UsersModel;
import org.mapstruct.Mapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;


public class UsersMapper implements FieldSetMapper<UsersModel> {


    public UsersModel mapFieldSet(FieldSet fieldSet) throws BindException {

        UsersModel user = new UsersModel();
        user.setId(fieldSet.readInt(0));
        user.setName(fieldSet.readString(1));
        user.setSalary(fieldSet.readDouble(2));



        return user;

    }

}
